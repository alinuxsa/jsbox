// 从相册选择图片
// 利用blur模糊图片


$ui.render({
    props: {
        title: "控件测试"
    },
    views: [
        {
            type: "image",
            props: {
                id: "img",
            },
            views: [{
                type: "blur",
                props: {
                    id: "bb",
                    style: 3,
                    // alpha: 0
                },
                layout: $layout.fill
            }],

            layout: function(make,view){
                make.left.top.equalTo(10)
                make.size.equalTo($size(240,160))
            }
        },
          {
            type: "slider",
            props: {
                id : "slider_alpha",
                value: 0.3,
                max: 1,
                min: 0
            },
            layout: (make, view) => {
                make.left.equalTo(20)
                make.top.equalTo(230)
                make.width.equalTo(250)
            },
            events: {
                changed: function(sender){
                    $("bb").alpha = sender.value
                    console.log($("bb").alpha)
                }
            }
        },
        {
            type: "button",
            props: {
                id: "btn1",
                title: "选择图片"
            },
            layout: function(make, view) {
                make.left.equalTo(20)
                make.top.equalTo(280)
                make.size.equalTo($size(160,80))
            },
            events: {
                tapped: function(sender){
                    $photo.pick({
                        format: "data",
                        handler: function(image) {
                          $("img").data = image.data
                        }
                      })
                }
            }
        }
]});


