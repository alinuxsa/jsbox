//列表 示例



$ui.render({
    props: {
        title: "列表测试"
    },
    views: [{
        type: "list",
        props: {
            id: "lst1",
            template: {
                props: {
                    bgcolor: $color("clear") //透明
                },
                views: [
                    {
                        type: "label",
                        props: {
                            id: "label",
                            bgcolor: $color("green"),
                            textColor: $color("red"),
                            align: $align.center,
                            font: $font(32)
                        },
                        layout: $layout.fill
                    }
                ]
            },
            data: [
                {
                  label: {
                    text: "Hello"
                  }
                },
                {
                  label: {
                    text: "World"
                  }
                }
              ]
    },
    layout: $layout.fill
    },{
        type: "button",
        props: {
            title: "修改list数据"
        },
        layout: make => {
            make.left.equalTo(20)
            make.top.equalTo(300)

        },
        events: {
            tapped: () => {
                var data = []
                for(let i=0;i<5;i++){
                    data.push({label: {text: i.toString()}})
                 }
               $("lst1").data = data
            }
        }
    }
]
});