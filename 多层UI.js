// 多层UI示例

$ui.render({
    props: {
        title: "测试多层UI"
    },
    views: [{
        type: "label",
        props: {
            id: "lab1",
            text: "第一层UI",
            align: $align.center,
            bgcolor: $color("green")
        },
        layout: make => {
            // make.top.left.right.bottom.equalTo(0)
            make.top.left.equalTo(20)
            make.size.equalTo($size(180,180))
        }
    },
    {
        type: "button",
        props: {
            id: "btn1",
            title: "点击加载第二层UI"
        },
        layout: make => {
            make.left.equalTo(20)
            make.top.equalTo(210)
            make.size.equalTo($size(180,50))
        },
        events: {
            tapped: sender => {
                console.log("加载第二层UI")
                renderSecond()
            }
        }
    }
]
});


function renderSecond(){
    $ui.push({
        props: {
            title: "第二层UI"
        },
        views: [{
            type: "label",
            props: {
                id: "lab2",
                bgcolor: $color("red"),
                text: "这是第二层UI"
            },
            layout: make => {
                // make.top.left.right.bottom.equalTo(0)
                make.top.left.equalTo(50)
                make.size.equalTo(160,160)
            },
            events: {
                
            }
        }]
    });
}