$ui.render({
    props: {
        title: "指示器测试"
    },
    views: [
        {
        type: "label",
        props: {
            // bgcolor: $color("green"),
            text: "指示器测试",
            align: $align.center
        },
        layout: $layout.fill
        },
        {
            type: "spinner",
            props: {
                loading:true,
                // bgcolor: $color("red")
            },
            layout: function(make,view) {
                make.center.equalTo(view.super).offset(30)
            }

        },
        {
            type: "button",
            props: {
                title: "按钮"
            },
            layout: function(make, view) {
                make.center.equalTo(view.super).offset(-30)
            },
            events: {
                tapped: function(sender){
                    console.log(sender.title)
                }
            }
        }
    ]
});



setTimeout(function(){$("spinner").loading = false}, 3000)