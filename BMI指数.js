'use strict'

var myapp = {};

function bmi() {
    ret = {};
    // 计算BMI指数 体重/(身高*身高)
    v = myapp.weight / (myapp.height * myapp.height);
    if (isNaN(v)) {
        ret.message = "请输入正确的身高体重!";
        ret.color = "##F6AF0C";
    } else if (v < 18.5) {
        ret.message = "过轻";
        ret.color = "#FAD727";
    } else if (v < 25) {
        ret.message = "正常";
        ret.color = "#CDFA27";
    } else if (v < 28) {
        ret.message = "过重";
        ret.color = "#FFC300";
    } else if (v < 32 ) {
        ret.message = "肥胖";
        ret.color = "#C70039";
    } else {
        ret.message = "严重肥胖";
        ret.color = "#900C3F";
    }
    return ret;
}

$ui.render({
    props: {
        title: "计算BMI指数"
    },
    views: [
        {
        type: "label",
        props: {
            id: "label1",
            text: "体重"
        },
        layout: function(make, view) {
            make.left.equalTo(10);
            make.top.equalTo(10);
            make.size.equalTo($size(40,30));
        }
        },
        {
        type: "label",
        props: {
            id: "label2",
            text: "身高"
        },
        layout: function(make, view) {
            make.left.equalTo(10);
            make.top.equalTo(60);
            make.size.equalTo($size(40,30));
        }
        },
        {
        type: "label",
        props: {
            id: "lab_msg",
            // text: "信息会显示在这里",
            // bgcolor: $color("gray")
        },
        layout: function(make, view) {
            make.size.equalTo($size(240,80));
            make.top.equalTo(120);
            make.left.equalTo(30);
        }
        },
        {
        type: "input",
        props: {
            id: "inWeight",
            // type: $kbType.search,
            // darkKeyboard: true,
            text: '',
            placeholder: "输入体重 单位KG"
            // darkKeyboard: true
        },
        layout: function(make, view) {
            make.left.equalTo(60);
            make.top.equalTo(10);
            make.size.equalTo($size(150, 32));
        },
        events:{
            changed: function(sender) {
                // console.log(sender.text)
                myapp.weight = sender.text;
            }
        }
        },
        {
            type: "input",
            props: {
                id: "inHeight",
                text: '',
                placeholder: "输入身高 单位米"
            },
            layout: function(make, view) {
                make.left.top.equalTo(60);
                // make.left.equalTo(60)
                make.size.equalTo($size(150,32));
            },
            events: {
                changed: function(sender) {
                    myapp.height = sender.text;
                }
            }
        },
        {
            type: "button",
            "props": {
                title: "计算",
                align: $align.center
            },
            "layout": function(make, view) {
                make.size.equalTo($size(65,65));
                make.left.equalTo(240);
                make.top.equalTo(20);
            },
            events:{
                tapped: function(sender) {
                    let ret = bmi()
                    lab_msg = $ui.get("lab_msg");
                    lab_msg.text = ret.message;
                    lab_msg.textColor = $color(ret.color);

                    // $ui.alert({
                    //     title: "BMI计算结果",
                    //     message: message,
                    // });
                }
            }
        }
    ]
});

