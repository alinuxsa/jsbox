// 仿照pythonista的ColorMixer

// var rgb = []
var r, g, b;

$ui.render({
  props: {
    title: "ColorMixer"
  },
  views: [
    {
      type: "slider",
      props: {
        id: "sr",
        value: r,
        min: 0,
        max: 255,
        minColor: $color("red")
      },
      layout: function(make, view) {
        make.left.equalTo(15);
        make.top.equalTo(60);
        make.width.equalTo(280);
      },
      events: {
        changed: function(sender) {
          r = Math.round(sender.value);
          shuffle();
        }
      }
    },
    {
      type: "slider",
      props: {
        id: "sg",
        value: g,
        min: 0,
        max: 255,
        minColor: $color("green")
      },
      layout: function(make, view) {
        make.left.equalTo(15);
        make.top.equalTo(100);
        make.width.equalTo(280);
      },
      events: {
        changed: function(sender) {
          g = Math.round(sender.value);
          shuffle();
        }
      }
    },
    {
      type: "slider",
      props: {
        id: "sb",
        value: b,
        min: 0,
        max: 255,
        minColor: $color("blue")
      },
      layout: function(make, view) {
        make.top.equalTo(135);
        make.left.equalTo(15);
        make.width.equalTo(280);
      },
      events: {
        changed: function(sender) {
          b = Math.round(sender.value);

          shuffle();
        }
      }
    },
    {
      type: "button",
      props: {
        title: "Shuffle"
      },
      layout: function(make, view) {
        make.left.equalTo(15);
        make.top.equalTo(180);
        make.size.equalTo($size(110, 45));
      },
      events: {
        tapped: function(sender) {
          r = g = b = "false";
          shuffle();
        }
      }
    },
    {
      type: "button",
      props: {
        title: "Copy"
      },
      layout: function(make, view) {
        make.left.equalTo(180);
        make.top.equalTo(180);
        make.size.equalTo($size(110, 45));
      },
      events: {
        tapped: function(sender) {
          $clipboard.text = $("label2").text;
          $ui.toast(`颜色值 ${$("label2").text} 已复制到剪切板`);
        }
      }
    },
    {
      type: "label",
      props: {
        id: "label1"
      },
      layout: function(make, view) {
        make.left.equalTo(15);
        make.top.equalTo(10);
        make.size.equalTo($size(150, 40));
      }
    },
    {
      type: "label",
      props: {
        id: "label2",
        align: $align.center,
        autoFontSize: true
      },
      layout: function(make, view) {
        make.top.equalTo(10);
        make.left.equalTo(170);
        make.size.equalTo($size(150, 40));
      }
    }
  ]
});

function random_int() {
  return Math.floor(Math.random() * 255 + 1);
}

function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

// const rgbToHex = (r, g, b) => '#' + [r, g, b].map(x => {
//   const hex = x.toString(16)
//   return hex.length === 1 ? '0' + hex : hex
// }).join('')

function shuffle() {
  if (isNaN(r + g + b)) {
    $("sr").value = r = random_int();
    $("sg").value = g = random_int();
    $("sb").value = b = random_int();
    vHex = rgbToHex(r, g, b);
    $("label1").bgcolor = $color(vHex);
    $("label2").text = vHex;
  } else {
    vHex = rgbToHex(r, g, b);
    $("label1").bgcolor = $color(vHex);
    $("label2").text = vHex;
  }
}

shuffle();
